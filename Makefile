.ONESHELL:
SHELL=/bin/bash

PROCESSORS = $(shell nproc)

BUILD_DIR	:= ./build
DOXYGEN 	:= doxygen
DOXYCFG 	:= ./doc/Doxyfile
BUILDDOX    := $(DOXYGEN) $(DOXYCFG)
DOXY_DIR    := ./doc/html
DOCS		:= $(DOXY_DIR)/index.html
RM			:= rm -rf

.PHONY: all doc clean

all: ut

deps:
	set -e
	if [ ! -d "$(BUILD_DIR)" ]; then mkdir $(BUILD_DIR); fi
	cd $(BUILD_DIR)

ut: deps
	set -e
	cd $(BUILD_DIR)
	cmake ../ -DBUILD_TESTS=ON
	make -j ${PROCESSORS}
	./ssm

install: deps
	set -e
	cd $(BUILD_DIR)
	cmake ../ -DBUILD_TESTS=OFF
	make install

format:
	set -e
	find include -name *.hpp -or -name *.h | xargs clang-format -i
	find test -name *.hpp -or -name *.h -or -name *.cpp | xargs clang-format -i

doc:
	@echo Generating docs...
	$(BUILDDOX) $<
	@echo "+------------------------------------------------------------------------+"
	@echo "| Docs generation finished. Open ./doc/html/index.html to view docs.     |"
	@echo "+------------------------------------------------------------------------+"

clean:
	$(RM) $(BUILD_DIR)
	$(RM) $(DOXY_DIR)
