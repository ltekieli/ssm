simple state machine
==

A simple, header only implementation of state pattern. The user needs to implement following interfaces to interact
with the state machine
```
struct state
{
  void on_entry();
  void on_exit();
}

struct event
{
  void on_dispatch();
}

struct guard
{
  void on_check(const event&);
}
```

Installation
--
```
make install
```

Running tests
--
```
make ut
```

Formatting
--
```
make format
```

Generating documentation
--
```
make doc
```

Contact
--
Lukasz Tekieli mail@ltekieli.com

