#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this
                          // in one cpp file
#include "catch.hpp"

#include "ssm.hpp"

using namespace ssm;

struct state
{
  void on_entry() { ++on_entry_executed; }
  void on_exit() { ++on_exit_executed; }

  unsigned on_entry_executed = 0;
  unsigned on_exit_executed = 0;
};

struct counter
{
  unsigned on_dispatch_executed = 0;
};

struct event
{
  void on_dispatch() {}
};

struct another_event : event
{
};

struct loopback_event : event
{
  loopback_event(state_machine& sm) : sm_(sm) {}

  void on_dispatch()
  {
    event::on_dispatch();
    sm_.dispatch(event());
  }

  state_machine& sm_;
};

TEST_CASE("State machine executes event")
{
  state_machine sm;
  state s1;

  sm.add_transition<event>(s1);
  sm.dispatch(event());

  REQUIRE(s1.on_entry_executed == 1);
  REQUIRE(s1.on_exit_executed == 0);
}

TEST_CASE("State machine does not dispatch if no transitions")
{
  state_machine sm;
  state s1;

  sm.dispatch(event());

  REQUIRE(s1.on_entry_executed == 0);
  REQUIRE(s1.on_exit_executed == 0);
}

TEST_CASE("State machine handles multiple states")
{
  state_machine sm;
  state s1, s2;

  sm.add_transition<event>(s1).add_transition<event>(s1, s2);
  sm.dispatch(event());
  sm.dispatch(event());

  REQUIRE(s1.on_entry_executed == 1);
  REQUIRE(s1.on_exit_executed == 1);
  REQUIRE(s2.on_entry_executed == 1);
  REQUIRE(s2.on_exit_executed == 0);
}

TEST_CASE("State machine handles multiple transitions")
{
  state_machine sm;
  state s1, s2;

  sm.add_transition<event>(s1).add_transition<event>(s1, s2).add_transition<event>(s2, s1);
  sm.dispatch(event());
  sm.dispatch(event());
  sm.dispatch(event());
  sm.dispatch(event());

  REQUIRE(s1.on_entry_executed == 2);
  REQUIRE(s1.on_exit_executed == 2);
  REQUIRE(s2.on_entry_executed == 2);
  REQUIRE(s2.on_exit_executed == 1);
}

TEST_CASE("State machine distinguishes transitions")
{
  state_machine sm;
  state s1, s2;

  sm.add_transition<event>(s1).add_transition<event>(s1, s2);

  for (unsigned int i = 0; i < 100; ++i)
  {
    sm.dispatch(event());
  }

  REQUIRE(s1.on_entry_executed == 1);
  REQUIRE(s1.on_exit_executed == 1);
  REQUIRE(s2.on_entry_executed == 1);
  REQUIRE(s2.on_exit_executed == 0);
}

TEST_CASE("State machine distinguishes transitions from same state")
{
  state_machine sm;
  state s1, s2;

  sm.add_transition<event>(s1)
      .add_transition<event>(s1, s2)
      .add_transition<event>(s2, s1)
      .add_transition<another_event>(s1, s2);

  sm.dispatch(event());

  REQUIRE(s1.on_entry_executed == 1);
  REQUIRE(s1.on_exit_executed == 0);
  REQUIRE(s2.on_entry_executed == 0);
  REQUIRE(s2.on_exit_executed == 0);

  sm.dispatch(event());

  REQUIRE(s1.on_entry_executed == 1);
  REQUIRE(s1.on_exit_executed == 1);
  REQUIRE(s2.on_entry_executed == 1);
  REQUIRE(s2.on_exit_executed == 0);

  sm.dispatch(event());

  REQUIRE(s1.on_entry_executed == 2);
  REQUIRE(s1.on_exit_executed == 1);
  REQUIRE(s2.on_entry_executed == 1);
  REQUIRE(s2.on_exit_executed == 1);

  sm.dispatch(another_event());

  REQUIRE(s1.on_entry_executed == 2);
  REQUIRE(s1.on_exit_executed == 2);
  REQUIRE(s2.on_entry_executed == 2);
  REQUIRE(s2.on_exit_executed == 1);
}

TEST_CASE("State machine handles event generation from inside of events on dispatch")
{
  state_machine sm;
  state s1, s2;

  sm.add_transition<event>(s1).add_transition<loopback_event>(s1, s2).add_transition<event>(s2, s1);
  sm.dispatch(event());
  sm.dispatch(loopback_event(sm));

  REQUIRE(s1.on_entry_executed == 2);
  REQUIRE(s1.on_exit_executed == 1);
  REQUIRE(s2.on_entry_executed == 1);
  REQUIRE(s2.on_exit_executed == 1);
}

TEST_CASE("State machine handles rvalue event")
{
  state_machine sm;
  state s1;

  sm.add_transition<event>(s1);
  sm.dispatch(event());

  REQUIRE(s1.on_entry_executed == 1);
  REQUIRE(s1.on_exit_executed == 0);
}

TEST_CASE("State machine handles same state transitions")
{
  state_machine sm;
  state s1;

  sm.add_transition<event>(s1).add_transition<event>(s1, s1);
  sm.dispatch(event());
  sm.dispatch(event());

  REQUIRE(s1.on_entry_executed == 2);
  REQUIRE(s1.on_exit_executed == 1);
}

struct true_guard
{
  bool on_check(const event&) const { return true; }
};

struct false_guard
{
  bool on_check(const event&) const { return false; }
};

TEST_CASE("State machine handles guarded transitions")
{
  state_machine sm;
  state s1;

  true_guard tg;
  false_guard fg;

  sm.add_guarded_transition<event>(s1, tg).add_guarded_transition<event>(s1, s1, fg);
  sm.dispatch(event());
  sm.dispatch(event());

  REQUIRE(s1.on_entry_executed == 1);
  REQUIRE(s1.on_exit_executed == 0);
}
