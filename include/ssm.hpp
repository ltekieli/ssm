
#pragma once
#ifndef SM_HPP_
#define SM_HPP_

#include <deque>
#include <type_traits>
#include <unordered_map>

namespace ssm
{
namespace detail
{

using type_id = void*;
template <typename T> inline type_id get_id()
{
  static int id;
  return &id;
}

inline bool no_guard_fn(const void*) { return true; }

} // namespace detail

/// Purpose of this class is to provide simple state machine. The interface
/// defines methods for adding transitions between states.
///
/// There are three main entities used in defining transitions:
///   - state - the entity defining machine vertex
///   - event - trigger for state transitions
///   - guard - check for state transitions
///
/// State should implement two methods:
///   - void on_entry()
///   - void on_exit()
///
/// Event should implement one method:
///   - void on_dispatch()
///
/// Guard should implement one method:
///   - bool on_check(const event&)
class state_machine final
{
  struct state_representation
  {
    const void* id;
    std::function<void()> on_entry;
    std::function<void()> on_exit;
  };

  struct transition_representation
  {
    state_representation start_state;
    state_representation end_state;
    std::function<bool(const void*)> guard;
  };

  enum class status
  {
    idle,
    dispatching
  };

public:
  /// Adds entry transition
  ///
  /// @tparam event event that triggers transition
  /// @param st state to which to transit to
  /// @return state_machine reference for method chaining
  template <typename event, typename state> state_machine& add_transition(state& st);

  /// Adds guarded entry transition
  ///
  /// @tparam event event that triggers transition
  /// @param st state to which to transit to
  /// @param grd guard that allows the transition to execute
  /// @return state_machine reference for method chaining
  template <typename event, typename state, typename guard>
  state_machine& add_guarded_transition(state& st, guard& grd);

  /// Adds transition between states
  ///
  /// @tparam event event that triggers transition
  /// @param st_st state to transit from
  /// @param st_en state to transit to
  /// @return state_machine reference for method chaining
  template <typename event, typename state1, typename state2>
  state_machine& add_transition(state1& st_st, state2& st_en);

  /// Adds guarded transition between states
  ///
  /// @tparam event event that triggers transition
  /// @param st_st state to transit from
  /// @param st_en state to transit to
  /// @param grd guard that allows the transition to execute
  /// @return state_machine reference for method chaining
  template <typename event, typename state1, typename state2, typename guard>
  state_machine& add_guarded_transition(state1& st_st, state2& st_en, guard& grd);

  /// Dispatch event to the machine
  ///
  /// @param ev event to dispatch
  template <typename event> void dispatch(event ev);

private:
  void add_transition(detail::type_id, state_representation st_st, state_representation st_en,
                      std::function<bool(const void*)> guard);

  template <typename state> state_representation get_state_representation(state& st);

  state_representation get_state_representation();

  template <typename event> void handle_when_idle(event ev);

  template <typename event> void handle_when_dispatching(event ev);

  void handle_queue();

  const void* current_state_ = nullptr;
  status status_ = status::idle;

  std::unordered_multimap<detail::type_id, transition_representation> transitions_;
  std::deque<std::function<void()>> queue_;
};

///
/// Public methods
///
template <typename event, typename state> inline state_machine& state_machine::add_transition(state& st)
{
  add_transition(detail::get_id<event>(), get_state_representation(), get_state_representation(st),
                 detail::no_guard_fn);
  return *this;
}

template <typename event, typename state, typename guard>
inline state_machine& state_machine::add_guarded_transition(state& st, guard& grd)
{
  add_transition(detail::get_id<event>(), get_state_representation(), get_state_representation(st),
                 [&grd](const void* ev) { return grd.on_check(*reinterpret_cast<const event*>(ev)); });
  return *this;
}

template <typename event, typename state1, typename state2>
inline state_machine& state_machine::add_transition(state1& st_st, state2& st_en)
{
  add_transition(detail::get_id<event>(), get_state_representation(st_st), get_state_representation(st_en),
                 detail::no_guard_fn);
  return *this;
}

template <typename event, typename state1, typename state2, typename guard>
inline state_machine& state_machine::add_guarded_transition(state1& st_st, state2& st_en, guard& grd)
{
  add_transition(detail::get_id<event>(), get_state_representation(st_st), get_state_representation(st_en),
                 [&grd](const void* ev) { return grd.on_check(*reinterpret_cast<const event*>(ev)); });
  return *this;
}

template <typename event> inline void state_machine::dispatch(event ev)
{
  static_assert(std::is_trivially_copyable<event>::value, "Event must be copyable");

  if (status_ == status::idle)
  {
    status_ = status::dispatching;
    handle_when_idle(ev);
    status_ = status::idle;
    handle_queue();
  }
  else
  {
    handle_when_dispatching(ev);
  }
}

///
/// Private methods
///
void state_machine::add_transition(detail::type_id tir, state_representation st_st, state_representation st_en,
                                   std::function<bool(const void*)> guard)
{
  transitions_.insert({tir, transition_representation{st_st, st_en, guard}});
}

template <typename state> state_machine::state_representation state_machine::get_state_representation(state& st)
{
  return state_representation{&st, [&st]() { st.on_entry(); }, [&st]() { st.on_exit(); }};
}

state_machine::state_representation state_machine::get_state_representation()
{
  return state_representation{nullptr, []() { return; }, []() { return; }};
}

template <typename event> inline void state_machine::handle_when_idle(event ev)
{
  auto range = transitions_.equal_range(detail::get_id<event>());
  auto current_transition = range.first;
  auto last_transition = range.second;

  for (; current_transition != last_transition; ++current_transition)
  {
    auto transition = current_transition->second;
    if (current_state_ == transition.start_state.id)
    {
      if (transition.guard(&ev))
      {
        if (transition.start_state.id != nullptr)
          transition.start_state.on_exit();
        ev.on_dispatch();
        current_state_ = transition.end_state.id;
        transition.end_state.on_entry();
        break;
      }
    }
  }
}

template <typename event> inline void state_machine::handle_when_dispatching(event ev)
{
  auto delegate = [this, &ev]() { dispatch(ev); };
  queue_.push_back(delegate);
}

void state_machine::handle_queue()
{
  if (!queue_.empty())
  {
    auto delegate = queue_.front();
    queue_.pop_front();
    delegate();
  }
}

} // namespace ssm

#endif // SM_HPP_
